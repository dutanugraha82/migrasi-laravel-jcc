<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist(){
        return view('register');
    }

    public function welcome(Request $request){
        // dd($request->all());

        $name1 = $request['firstName'];
        $name2 = $request['lastName'];
        return view('welcome', compact('name1','name2'));
    }

}
