<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Pendaftaran</title>
</head>
<body>
    <h1>Buat Akun Baru</h1>
    <h2>Sign Up Form</h2>


    <form action="/post" method="POST">
    @csrf

        <label for="">First Name :</label>
        <br>
        <input type="text" name="firstName">
        <br>
        <br>
        <label for="">Last Name :</label>
        <br>
        <input type="text" name="lastName">
        <br>
        <br>
        <label for="">Gender</label>
        <br>
        <br>
        <input type="radio" name="Male" id="">Male <br>
        <input type="radio" name="Female" id="">Female <br>
        <br>
        <br>
        <label for="">Nationality</label>
        <br>
        <br>
        <select name="nationality" id="">
            <option value="Indoesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="English">English</option>
        </select>
        <br>
        <br>
        <label for="">Language Spoken</label> <br>
        <input type="checkbox" value="">Indonesia <br>
        <input type="checkbox" value="">English <br>
        <input type="checkbox" value="">Other <br>
        <br>
        <br>
        <label for="">Bio</label><br>
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <br>
            <button type="">Sign Up</button>

    </form>

</body>
</html>